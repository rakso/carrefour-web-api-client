# Carrefour WEB API client

## Description

Very simple Carrefour WEB API client written in TypeScript. Currently, this client can:

- Return info about the product (Polish dataset) via barcode (EAN) or product name.

## Installation

```bash
npm i carrefour-web-api-client # or yarn add carrefour-web-api-client
```

## Usage

Get info about product `5900972012597`:

```javascript
import { CarrefourWebApiClient } from 'carrefour-web-api-client';

const carrefourWebApiClient = new CarrefourWebApiClient();

carrefourWebApiClient.getProductInfoByBarcode('5900972012597').then((products: any) => {
  if (products === null) {
    console.log(">> Can't find this product.");
  } else {
    const {
      name,
      defaultCategoryName,
      actualSku: {
        amount: { actualGrossPriceString },
      },
    } = products[0];

    console.log(`>> Found "${name}" of category "${defaultCategoryName}"! Actual price: ${actualGrossPriceString}`);
  }
});
```

To run this example install package, copy and paste above example into file named `app.js` and then run:

```bash
node --require ts-node/register --require source-map-support/register app.ts
```

## Support

Please report any bugs [here](https://gitlab.com/rakso/carrefour-web-api-client/-/issues/new).

## Roadmap

Currently, this project does all that I need, so I think that new features won't be added.

## Contributing

Feel free to fork this project and made some changes, any upgrades will be much appreciated.

## Authors

- Oskar Jaskólski

## License

MIT

## Project status

This project is written to be as simple as it can be. It was created for me just for getting info about products. But who knows, maybe in future it will do more, but currently it will be as it is.
