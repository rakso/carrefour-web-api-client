import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';

import { CarrefourWebApiClient } from '.';

const carrefourWebApiClient = new CarrefourWebApiClient();

async function example() {
  const rl = readline.createInterface({ input, output });
  const barcodeToSearch = await rl.question('Type in barcode of product to search info for > ');
  rl.close();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  carrefourWebApiClient.getProductInfoByBarcode(barcodeToSearch).then((products: any) => {
    if (products === null) {
      console.log(">> Can't find this product.");
    } else {
      products.forEach((product: any) => {
        const {
          name,
          defaultCategoryName,
          actualSku: {
            amount: { actualGrossPriceString, actualOldPriceString },
          },
        } = product;

        console.log(
          `>> Found "${name}" of category "${defaultCategoryName}"! Actual price: ${actualGrossPriceString}; Old price: ${actualOldPriceString};`,
        );
      });
    }
    example();
  });
}

example();
