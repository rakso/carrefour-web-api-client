/* eslint-disable @typescript-eslint/no-explicit-any */
import { default as axios, AxiosResponse, AxiosError } from 'axios';

class ResponseError extends Error {
  constructor(msg: string) {
    super(msg);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ResponseError.prototype);
  }
}

interface CarrefourWebApiClientOptions {
  verbose: boolean;
}

interface SearchProductParams {
  resolveCategories?: boolean;
  resolveBrands?: boolean;
  resolveProductLabels?: boolean;
  resolveAttributes?: boolean;
  resolveCreateDate?: string;
  forceSearch?: boolean;
}

export class CarrefourWebApiClient {
  private verbose = false;
  private cookies?: string;
  private getSessionCookiesUrl: string;
  private getProductInfoUrl: string;
  private getSessionRequestOptions: Record<string, string | Record<string, string>>;
  private getProductRequestOptions: any;
  private lastResponse?: AxiosResponse;
  private isWaitingForSession = false;

  public constructor(options: CarrefourWebApiClientOptions = { verbose: false }) {
    if (options.verbose) {
      this.verbose = true;
    }

    this.getSessionCookiesUrl = 'https://www.carrefour.pl/';
    this.getProductInfoUrl = 'https://www.carrefour.pl/web/catalog';

    this.getSessionRequestOptions = {
      method: 'HEAD',
      headers: {
        accept: '*/*',
      },
    };

    this.getProductRequestOptions = {
      method: 'GET',
      headers: {
        accept: 'application/json',
        connection: 'keep-alive',
      },
    };
  }

  static parseCookies(response: AxiosResponse) {
    const raw = response.headers['set-cookie'];
    if (raw === undefined) {
      return '';
    }

    return raw
      .map((entry: any) => {
        const parts = entry.split(';');
        const cookiePart = parts[0];
        return cookiePart;
      })
      .join(';');
  }

  async makeRequest(url: URL, options: Record<string, string | Record<string, string>>) {
    const optionsWithUrl = { url: url.toString(), ...options };
    this.lastResponse = await axios.request(optionsWithUrl);
    return this.lastResponse;
  }

  async getNewSessionCookies() {
    await this.makeRequest(new URL(this.getSessionCookiesUrl), this.getSessionRequestOptions);
    this.cookies = CarrefourWebApiClient.parseCookies(this.lastResponse as AxiosResponse);
  }

  async makeGetProductInfoRequest(eanCode: string, params: SearchProductParams): Promise<any> {
    const productInfoUrlWithQueries = new URL(this.getProductInfoUrl);
    const paramsWithSearchData = { search: eanCode, ...(params as Record<string, string>) };

    productInfoUrlWithQueries.search = new URLSearchParams(paramsWithSearchData).toString();

    const headers = {
      ...this.getProductRequestOptions.headers,
      cookie: this.cookies,
    };

    try {
      this.lastResponse = await this.makeRequest(productInfoUrlWithQueries, {
        method: this.getProductRequestOptions.method,
        headers,
      });
    } catch (error) {
      if (error instanceof AxiosError) {
        throw new ResponseError(`Wrong resposne code ${error.response?.status} ${error.response?.statusText}!`);
      }
      throw error;
    }

    const response: any = this.lastResponse.data;
    if (response.totalCount > 0) {
      return response.content;
    } else {
      return null;
    }
  }

  async getProductInfoByBarcode(eanCode: string, searchParams: SearchProductParams = {}): Promise<any> {
    try {
      return await this.makeGetProductInfoRequest(eanCode, searchParams);
    } catch (error) {
      if (error instanceof ResponseError) {
        if (this.isWaitingForSession) {
          throw new Error(`Can't make product info request! ${error}. Propably session is wrong?"`);
        }
      } else {
        throw error;
      }
      this.isWaitingForSession = true;
      if (this.verbose) {
        console.log('Old session, requesting new...');
      }
      await this.getNewSessionCookies();
      return this.getProductInfoByBarcode(eanCode, searchParams);
    }
  }
}
